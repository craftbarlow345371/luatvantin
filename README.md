# Luật Vạn Tín

Luật Vạn Tín cung cấp thủ tục, tại liệu, biểu mẫu, thư viện luật mới nhất bạn sẽ được tư vấn các thủ tục pháp lý thật chuyên nghiệp, uy tín và hiệu quả đầy đủ nhất.

- Địa chỉ: Số 7, đường số 14, Khu đô thị mới Him Lam, Phường Tân Hưng, Quận 7, TP Hồ Chí Minh.

- SDT: 0938086558

Giấy phép đăng ký hoạt động số: 41.02.1440/ĐKHĐ do Sở Tư pháp TP.HCM cấp lần đầu ngày 15/3/2011, cấp đổi ngày 30/08/2017
Ngày cấp: 2011-03-15
Nơi cấp: Hồ Chí Minh

Chuẩn bị giai đoạn xét xử và xét xử sơ thẩm:
a. Nhận đơn khởi kiện và thụ lý đơn:
– Nhận và xử lý đơn khởi kiện (căn cứ tại điều 191 Bộ luật tố tụng dân sự năm 2015):
Trước khi tiến hành xử lý vụ kiện, quan toà bắc bắt buộc coi xét đơn khởi kiện để công nhận tính đúng đắn của vụ việc và xem xét có quyết định buộc phải sửa đổi , bổ sung đơn kiện không. Sau đấy, tiến hành thủ lý vụ án theo giấy má thường ngày hay rút gọn;
Trường hợp vụ án ko thuộc thẩm quyền giải quyết của Tòa án thì chuyển đơn kiện cho tòa án sở hữu thẩm quyền xử lý đồng thời thông tin cho người khởi kiện;
Trường hợp vụ việc ko thuộc thẩm quyền của Tòa án thì trả lại đơn cho người gửi.
– quy trình thụ lý đơn khởi kiện (căn cứ tại điều 195 bộ luật tố tụng dân sự năm 2015):
Sau lúc nhận thủ tục khởi kiện, giả dụ thấy đáp ứng phần đông những yêu cầu để tiến hành vụ án, thẩm phán thông tin ngay cho người khởi kiện để họ làm cho hồ sơ nộp tiền trợ thời ứng phí ví như cần;
Người khởi kiện phải nộp tiền trợ thì ứng án phí và biên lai thu tiền trong thời hạn 7 ngày trong khoảng ngày nhận được giấy báo của tào án; Và quan toà cũng bắt đầu thụ lý vụ án sau khi nhận biên lai thu tiền lâm thời ứng án phí;
Trường hợp người khởi kiện được miễn hoặc chẳng hề nộp tiền nhất thời ứng án phí, thẩm phán quyết định thụ lý vụ án ngay sau khi nhận được đa số hồ sơ khởi kiện.
b. Chuẩn bị xét xử và xét xử: Căn cứ theo Điều 203 bộ luật tố tụng dân sự năm 2015
– Thời hạn chuẩn bị xét xử vụ án mâu thuẫn đất đai là 04 tháng, vụ việc phức tạp được gia hạn không quá 02 tháng, như thế tổng thời gian là 6 tháng. Không những thế, nếu vụ án ko thuộc trường hợp trợ thời đình chỉ hoặc đình chỉ thì tòa án sẽ đưa ra xét xử;
– Sau khi mang bản án sơ thẩm các đối tác tranh chấp mang quyền kháng cáo giả dụ có căn cứ theo quy định.

https://thuviendoanhnghiep.vn/

https://www.facebook.com/luatvantin.com.vn/

https://www.flickr.com/people/196227544@N07/

https://www.twitch.tv/luatvantin1/about
